import { NgModule } from '@angular/core';
import { EditProfileformComponent } from './edit-profileform/edit-profileform.component';
@NgModule({
	declarations: [EditProfileformComponent],
	imports: [],
	exports: [EditProfileformComponent]
})
export class ComponentsModule {}
