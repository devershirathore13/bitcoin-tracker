import { Component } from '@angular/core';

/**
 * Generated class for the EditProfileformComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'app-edit-profileform',
  templateUrl: 'edit-profileform.component.html'
})
export class EditProfileformComponent {

  text: string;

  constructor() {
    console.log('Hello EditProfileformComponent Component');
    this.text = 'Hello World';
  }

}
